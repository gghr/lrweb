import React from 'react';
import Card from '../components/Card/Index';
import AppContext from '../context'
function Home ( { items, cartItems, searchValue, setSearchValue, onChangeSearchInput, onAddToCart}){

    return(
        <div className="content p-40">
        <div className="d-flex align-center justify-between mb-40">
          <h1>{searchValue ? `Поиск по запросу: "${searchValue}"` : 'Все товары'}</h1>
          <div className="search-block d-flex">
      
            <img  src="/img/lyp.jpg" alt='Lypa'/>
            {searchValue && 
            <img onClick={() => setSearchValue('')}
             className='clear cu-p' src="/img/x.jpg" alt='X' />}
            <input onChange={onChangeSearchInput} value={searchValue} placeholder="Поиск..." />
          </div>
        </div>

        <div className="d-flex flex-wrap">
       
       
         
        {items.filter(item => item.title.toLowerCase().includes(searchValue.toLowerCase()))
        .map((item, index)=>(
          <Card key={index}
           
           onFavorite={() => console.log('')}
            onPlus={(obj) => onAddToCart(obj)}
            
            {...item}
             />
        
        ))}
      </div>
      </div>
    );
}
export default Home;