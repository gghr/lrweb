import axios from "axios";
import AppContext from "../context";

import React from "react";
import Card from "../components/Card/Index";

function Orders (){
  const {onAddToCart } = React.useContext(AppContext);
  const [orders, setOrders] = React.useState([]);
  React.useEffect(() => {
    (async () => {
      try {
        const { data } = await axios.get('http://localhost:3001/order');
        setOrders(data.reduce((prev, obj) => [...prev, ...obj.items], []));
      } catch (error) {
        alert('');
        console.error(error);
      }
    })();
  }, []);
    return(
        <div className="content p-40">
        <div className="d-flex align-center justify-between mb-40">
          <h1>Заказы</h1>
      
        </div>

        <div className="d-flex flex-wrap">
          {orders.map((item, index) => (
            <Card
            key={index}
            {...item} />
          ))}
      </div>
      </div>
    );
}
export default Orders;