import React from 'react';
import axios from 'axios';
import {Routes, Route} from 'react-router-dom'
import Home from './pages/Home';
import Drawer from './components/Drawer';
import Header from './components/Header';
import Orders from './pages/Orders';
import AppContext from './context'
import Register from './pages/Register';




function App() {
  const [items, setItems] = React.useState([]);
  const [cartItems, setCartItems] = React.useState([]);
  const [searchValue, setSearchValue] = React.useState(' ');
  const [cartOpened, setCartOpened] = React.useState(false)
//.
  React.useEffect(() => {
    async function fetchData() {
      const cartResponse = await axios.get('http://localhost:3001/cart')
      const itemsResponse = await axios.get('http://localhost:3001/items')

      setCartItems(cartResponse.data);
      setItems(itemsResponse.data);
    }
    fetchData();
  },[]);





const onAddToCart = (obj) => {
  if (cartItems.find((item) => Number(item.id) == Number(obj.id))) {
    axios.delete(`http://localhost:3001/cart/${obj.id}`);
    setCartItems((prev) => prev.filter((item) => Number(item.id) !== Number(obj.id)));
  } else {
  axios.post('http://localhost:3001/cart', obj)
    setCartItems((prev) => [...prev,obj]);
  //.then(res =>setCartItems(prev => [...prev, res.data])) 
  }
};



const onRemoveItem = (id) =>{
    axios.delete(`http://localhost:3001/cart/${id}`);
  
 
  
  setCartItems((prev) => prev.filter((item) => item.id !== id));
};




const onChangeSearchInput = (event) => {
  setSearchValue(event.target.value);
}

const isItemAdded = (id)=>{
  return cartItems.some((obj) =>Number(obj.id) == Number(id));
};
  
 
  return (
    <AppContext.Provider value={{items, cartItems, isItemAdded, setCartOpened, setCartItems}}>
    <div className="wrapper clear">
      {cartOpened && <Drawer items={cartItems} onClose={() => setCartOpened(false)} onRemove={onRemoveItem}  />}
      <Header onClickCart={() => setCartOpened(true)} />

      
        <Routes>
          <Route
          path="/"
          exact
          element={
            <Home
              items={items}
              searchValue={searchValue}
              setSearchValue={setSearchValue}
              onChangeSearchInput={onChangeSearchInput}
              cartItems={cartItems}
              onAddToCart={onAddToCart}
              
            />
          }
          />
        
          <Route path='/orders' exact element={<Orders/>}
          />
           <Route path='/register' exact element={<Register/>}
          />
        </Routes>
       


  

          
          
              
           
          
        





      

      
    </div>
    </AppContext.Provider>
 
  )
}

export default App;
