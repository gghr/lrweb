import AppContext from "../context";
import Info from "./Info";
import React from "react";
import axios from "axios";

function Drawer({onClose, onRemove, items =[] }) {
  const {cartItems, setCartItems} = React.useContext(AppContext)
  const [orderId, setOrderId] =React.useState(null)
  const [isOrderComplete, setIsOrderComplete] = React.useState(false)
  const totalPrice = cartItems.reduce((sum, obj) => obj.price + sum, 0)

  const onClickerOrder = async () => {
    
      const {data} = await axios.post('http://localhost:3001/order',{
        items: cartItems,
      })
      setOrderId(data.id)
      setIsOrderComplete(true)
     setCartItems([])
    
    
}
    return(
        <div className="overlay" >
        <div className="drawer">
        <h2 className="d-flex justify-between mb-30">Корзина{' '}
        <img onClick={onClose} className="cu-p" src="/img/x.jpg" alt="X" /></h2>


        {
          items.length > 0 ? (
             <div className="d-flex flex-column flex">
            <div className="items flex">
          {items.map((obj) => (

            

            <div key={obj.id} className="cartItem d-flex align-center mb-20">
             
                <div
                  style={{ backgroundImage: `url(${obj.imageUrl})`}}
                 ></div>
            
            <img width={133} height={112} src={obj.imageUrl} alt="1"/>
          
            <div className="mr-20 flex">
            <p className="mb-5">{obj.title}</p>
            <b>{obj.price} руб.</b>
            </div>
            <img onClick={() => onRemove(obj.id)} className="removeBtn" src="/img/x.jpg" alt="X" />
            </div>

        ))}
          
        </div>
        <div className="cartTotalBlock">
        <ul>
        <li>
          <span>Итого:</span>
          <div></div>
          <b>{totalPrice} руб. </b>
        </li>
      
        </ul>
        <button onClick={onClickerOrder} className="gButton">Оформить заказ</button>
        </div>
              

      </div>
        ) : ( 
          <Info
            title={isOrderComplete ? 'Заказ оформлен!' : 'Корзина пуста'}
            description={
              isOrderComplete
                ? `Ваш заказ #${orderId} скоро будет передан курьерской доставке`
                : 'Добавьте хотя бы что-то, чтобы сделать заказ'
            }
            image={isOrderComplete ? 'img/da.jpg' : 'img/box.jpg'}
          />
        )}
      </div>
    </div>
  );
}

export default Drawer;