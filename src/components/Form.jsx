import axios from "axios";
import React, {useContext, useState } from "react"
import { Link, useNavigate } from "react-router-dom";
import {FaPencilAlt} from 'react-icons/fa'
import {AiFillEye, AiFillEyeInvisible} from 'react-icons/ai'
import CustomContext from "../utils/Context";
import { ToastContainer, toast } from "react-toastify";
import'react-toastify/dist/ReactToastify.css'


const Form = () => {
    const [status, setStatus] = useState(false)
    const[email, setEmail] = useState( "")
    const navigate = useNavigate()
    const [eye, setEye] = useState(false)
    //.
    const {user, setUser} = useContext(CustomContext) || {} ;
    const registerUser = (e) => {
        e.preventDefault()
        
        let newUser ={
            email,
            password: e.target[0].value}
            axios.post('http://localhost:3001/register' ,newUser)

            .then (({data}) => {
                setUser({
                    token: data.accessToken,
                    ...data.user
                })
                localStorage.setItem('user', JSON.stringify({
                    token: data.accessToken,
                    ...data.user
    
                }))
                navigate('/')
               
                })
        
  
                                }

    const notify = () => toast.success("Аккаунт успешно создан!" ,{position:'top-center', theme:'colored'});
       
    return (
        <div className="con">
        <form action="" className="form" onSubmit={registerUser}>
        {
             status && <p className='form__email' onClick={() => setStatus(false)}> {email}<FaPencilAlt/> </p> }

                <h2 className="form__title"> {
            status ? 'Введите пароль' : 'Регистрация'
        }
         </h2>

{
    status && <>
    <div children='form__password'>
    <input className="form__field" placeholder="Введите пароль"type={eye ? "text" : 'password'}/>
    <span className="form__eye" onClick={() => setEye(prev => !prev)}>
    {
            eye ? <AiFillEyeInvisible/> : <AiFillEye/>
        }
        


    </span>
    </div>
        <button onClick={notify} className="form__btn" type='submit'>Создать аккаунт </button><ToastContainer />
        </> 
 }
 {
    !status &&
  <>
        <input value={email} onChange={(e) => setEmail(e.target.value)} className="form__field" placeholder="Введите email" type="text"/>
        <div onClick={() => setStatus(true)} className="form__btn">Далее</div>
        
        </>
}

        </form>
        </div>
    )
}
export default Form
