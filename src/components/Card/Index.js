import React from 'react';
import styles from './Card.module.scss'
import AppContext from '../../context';

function Card({id, title, imageUrl, price, onPlus }) {
  const {isItemAdded } = React.useContext (AppContext)

  const onClickPlus = () => {
    onPlus({id, title, imageUrl, price});
  }

   
    return(
    <div className={styles.card}>
   

    <img width={133} height={112} src={imageUrl} alt="1"/>
    <h5>{title}</h5>
    <div className="d-flex justify-between align-center">
      <div  className='d-flex flex-column '>
      <span>Цена</span>
      <b>{price} руб.</b>
    </div>
    
      {onPlus && <img className={styles.plus} onClick={onClickPlus} src={isItemAdded(id) ? "img/gal.jpg" : "/img/plus.jpg"} alt="" />} 
    
    </div>
  
  </div>



    );
}
    export default Card;
