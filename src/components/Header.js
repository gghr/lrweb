import {Link} from 'react-router-dom'
import AppContext from '../context';
import React  from 'react';
 
function Header(props){

  const {cartItems}=React.useContext(AppContext)
  const totalPrice = cartItems.reduce((sum, obj) => obj.price + sum, 0)

return(
    <header className="d-flex justify-between align-center p-40">
      <Link to="/">
        <div className="d-flex align-center">
        <img width={100} height={100} src="/img/log.png" alt="Logo" />
          <div >
            <h3 className="text-uppercase">topsport</h3>
            <p className="opacity-5">Лучший магазин спортивной одежды</p>
          </div>
        </div>
        </Link>
        <ul className="d-flex">
          <li onClick={props.onClickCart} className="mr-30 cu-p">
            <img width={18} height={18} src="/img/korzina.jpg" alt="Korzina" />
            <span>{totalPrice} руб.</span>
          </li>
          <li>
            <Link to="/orders">
            <img width={18} height={18} src="/img/li.jpg" alt="Li" />
            </Link>

            <Link to="/register">
            <img width={18} height={18} src="/img/lk.jpg" alt="L" />
            </Link>
          </li>
        </ul>
      </header>

);
}
export default Header;